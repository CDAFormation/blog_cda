<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckValidIp
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
private $whiteListe = ["127.0.1.1", "127.0.2.1"];
    public function handle(Request $request, Closure $next, $ip)
    {
        if (in_array($request->ip, $this->whiteListe) || $request->ip !== $ip) {
            return $next($request);
        } else {
            abort('403');
        }

    }
}
