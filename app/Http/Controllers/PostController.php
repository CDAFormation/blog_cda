<?php

namespace App\Http\Controllers;

use App\Events\PostCreated;
use App\Http\Requests\StorePostRequest;
use App\Listeners\LogCreatedPost;
use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    function index() {
        $posts = Post::select('id', 'title', 'content', 'slug', 'category_id', 'img_url', 'created_at', 'updated_at')
            ->with('category:id,title,description,created_at,updated_at')->get();
        return view('post.index', [
            'posts' => $posts
        ]);
    }

    function create() {
        $categories = Category::OrderBy('title')->pluck('title','id');
        $users = User::OrderBy('name')->pluck('name','id');
        $tags = Tag::OrderBy('title')->pluck('title','id');
        return view('post.addpost', [
            'categories' => $categories,
            'users' => $users,
            'tags' => $tags,
        ]);
    }

    function save(storePostRequest $request) {
        $post = Post::create($request->all());
        $post->tag()->sync($request->tags);

        $imageName = $post->slug.'.'.$request->image->extension();
        $request->image->storeAs("public",$imageName);

        $post->img_url = $imageName;
        $post->save();

        event(new PostCreated($post));

        return Redirect()->Route('post.index');
    }

    function delete(Post $post) {
        $post->delete();
        return Redirect()->route('post.index');
    }
}
