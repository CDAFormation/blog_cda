<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCategoryRequest;
use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class CategoryController extends Controller
{
    function list(){
        $categories = Category::all();
        return view('category.categories', [
            'categories' => $categories
        ]);
    }

    function create() {
        return view('category.addCategory');
    }

    function save(StoreCategoryRequest $request) {
        Category::create($request->all());
        return Redirect()->route('category.list');
    }

    function delete(Category $category) {
        $category->delete();
        return Redirect()->route('category.list');
    }

    function edit($id) {
        $category = Category::select('id','title','description')
            ->where('id', $id)->first();
//        dd($category);
        return view('category.editCategory', [
            'category' => $category
        ]);
    }

    function update(Request $request) {
        dd($request->all());
//        Post::find($category->id)->update($category);
//        Post::find($id)->update($request->all());
        Category::find($request->id)->update($request->all());
        return Redirect()->route('category.list');
    }

    function show($category) {
        $posts = Post::select('id', 'title', 'content', 'slug', 'category_id', 'created_at', 'updated_at')
            ->with('category:id,title,description,created_at,updated_at')
            ->where('category_id', $category)->get();

//        $category = Category::select('title')
//            ->where('id', $category)->first();
//        dd($category);

//        $category = $category->title;



        return view('category.showCategories', [
            'posts' => $posts
        ]);
    }
}
