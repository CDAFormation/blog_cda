    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container-fluid">
            <a class="navbar-brand" href="/index">Articles</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="/categories">Catégories</a>
                    </li>
                </ul>
            </div>
            @if (\Illuminate\Support\Facades\Auth::check())
                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                        {{ Auth::user()->name }}
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                        <li>
                            <form method="POST" action="{{ route('logout') }}">
                                @csrf

                                <x-responsive-nav-link :href="route('logout')"
                                                       onclick="event.preventDefault();
                                    this.closest('form').submit();">
                                    {{ __('Déconnexion') }}
                                </x-responsive-nav-link>
                            </form>
                        </li>
                    </ul>
                </div>
            @else
                <form method="POST" action="{{ route('login') }}">
                    @csrf

                    <x-responsive-nav-link :href="route('login')">
                        Connexion
                    </x-responsive-nav-link>
                </form>
            @endif


        </div>
    </nav>
