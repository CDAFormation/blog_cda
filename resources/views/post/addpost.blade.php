@extends('head')
<?php
//dd($categories);
//    dd($users);

?>

@section('content')

    @foreach ($errors->all() as $error)
        <div class="alert alert-danger">
        <li>{{ $error }}</li>
        </div>
    @endforeach

    <div class="container p-5 my-5 border">
            <h1 style="display: inline;">Ajouter un article</h1>
        <a href="/index" class="btn btn-danger" style="float: right;">Annuler</a>
    {!! Form::open(['route' => 'post.save', 'files' => true]) !!}

        <div class="row">
            {{ Form::label('user_id', 'Utilisateur') }}
        </div>
        <div class="row">
            {{ Form::select('user_id', $users) }}
        </div>
        <div class="row">
            {{ Form::label('category_id', 'Catégorie') }}
        </div>
        <div class="row">
                {{ Form::select('category_id', $categories) }}
        </div>
        <div class="row">
            {{ Form::label('title', 'Titre') }}
        </div>
        <div class="row">
            {{ Form::text('title') }}
        </div>
        <div class="row">
            {{ Form::label('content', 'Contenu') }}
        </div>
        <div class="row">
            {{ Form::textarea('content') }}
        </div>
        <div class="row">
            {{ Form::label('img_url', 'image') }}
        </div>
        <div class="row">
            {{ Form::text('img_url') }}
        </div>
        <div class="row">
            {{ Form::label('image', 'image') }}
        </div>
        <div class="row">
            {{ Form::file('image') }}
        </div>
        <div class="row">
            {{Form::label('tags', 'Tags')}}
        </div>
        <div class="row">
            {{Form::select('tags',$tags,null,array('class' => 'form-control', 'multiple'=>'multiple', 'name'=>'tags[]'))}}
        </div>
        <br>
        <div class="row">
            {{  Form::submit('Enregistrer', ['class' => 'btn btn-primary']) }}
        </div>
    {!! Form::close() !!}
    </div>
@endsection
