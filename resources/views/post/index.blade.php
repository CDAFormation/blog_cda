@extends('head')

<?php
//    dd($posts);
//var_dump($posts);
?>

@section('content')
    <div class="container p-5 my-5 border">
        <h1 style="display: inline;">Liste des articles</h1>
                <a href="/index/create" class="btn btn-primary" style="float: right;">Ajouter</a>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">id</th>
                <th scope="col">title</th>
                <th scope="col">slug</th>
                <th scope="col">catégorie</th>
                <th scope="col">created_at</th>
                <th scope="col">updated_at</th>
                <th scope="col">actions</th>
                <th scope="col">actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($posts as $post)
                <tr>
                    <th scope="row">{{ $post -> id }}</th>
                    <td>{{ $post -> title }}</td>
                    <td>{{ $post -> slug }}</td>
                    <td><a href="/categories/{{ $post -> category_id }}">{{ $post -> category -> title }}</a></td>
                    <td>{{ $post -> created_at }}</td>
                    <td>{{ $post -> updated_at }}</td>
                    <td>
                        {!! Form::open(['route' => ['post.delete', $post->id], 'method' => 'delete']) !!}
                            {{  Form::submit('Supprimer', ['class' => 'btn btn-danger']) }}
                        {!! Form::close() !!}
                    </td>
                    <td>
                        <img src="{{ asset("storage/".$post->img_url)}}" width="200" height="450">
                    </td>
                </tr>
            @endforeach


            </tbody>
        </table>
    </div>
@endsection
