@extends('head')

<?php
//dd($category);
//    var_dump($category);
//    echo $category;
//    echo $category->id;
?>

@section('content')
    <div class="container p-5 my-5 border">
            <h1 style="display: inline;">Modifier une catégorie</h1>
        <a href="/categories" class="btn btn-danger" style="float: right;">Annuler</a>
    {!! Form::open(['route' => 'category.update', 'method' => 'put']) !!}
        <div class="row">
            {{ Form::label('title', 'Titre') }}
        </div>
        <div class="row">
            {{ Form::text('title', $category->title) }}
        </div>
        <div class="row">
            {{ Form::label('description', 'Description') }}
        </div>
        <div class="row">
            {{ Form::textarea('description', $category->description) }}
        </div>
        <br>
        <div class="row">
            {{  Form::submit('Enregistrer', ['class' => 'btn btn-primary']) }}
        </div>
    {!! Form::close() !!}
    </div>
@endsection
