@extends('head')

<?php
?>

@section('content')
    <div class="container p-5 my-5 border">
        <h1 style="display: inline;">Liste des catégories</h1>
        <a href="/categories/create" class="btn btn-primary" style="float: right;">Ajouter</a>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">id</th>
                <th scope="col">title</th>
                <th scope="col">description</th>
                <th scope="col">actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($categories as $category)
                <tr>
                    <th scope="row">{{ $category -> id }}</th>
                    <td>{{ $category -> title }}</td>
                    <td>{{ $category -> description }}</td>
                    <td>
                        {!! Form::open(['route' => ['category.delete', $category->id], 'method' => 'delete']) !!}
                            {{  Form::submit('Supprimer', ['class' => 'btn btn-danger']) }}
                        {!! Form::close() !!}
                        {!! Form::open(['route' => ['category.edit', $category->id], 'method' => 'get']) !!}
                            {{  Form::submit('Editer', ['class' => 'btn btn-secondary']) }}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach


            </tbody>
        </table>
    </div>
@endsection
