@extends('head')

<?php
?>

@section('content')
    @foreach ($errors->all() as $error)
        <div class="alert alert-danger">
            <li>{{ $error }}</li>
        </div>
    @endforeach

    <div class="container p-5 my-5 border">
            <h1 style="display: inline;">Ajouter une catégorie</h1>
        <a href="/categories" class="btn btn-danger" style="float: right;">Annuler</a>
    {!! Form::open(['route' => 'category.save']) !!}
        <div class="row">
            {{ Form::label('title', 'Titre') }}
        </div>
        <div class="row">
            {{ Form::text('title') }}
        </div>
        <div class="row">
            {{ Form::label('description', 'Description') }}
        </div>
        <div class="row">
            {{ Form::textarea('description') }}
        </div>
        <br>
        <div class="row">
            {{  Form::submit('Enregistrer', ['class' => 'btn btn-primary']) }}
        </div>
    {!! Form::close() !!}
    </div>
@endsection
