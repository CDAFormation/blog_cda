<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('post.index');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';




//Route Posts


//Route::group(['middleware' => ['auth']], function () {
    Route::get('/index', [PostController::class, 'index'])->name('post.index')->middleware('CheckValidIp:127.0.0.1');
    Route::get('/index/create', [PostController::class, 'create'])->name('post.create');
    Route::delete('/index/{post}', [PostController::class, 'delete'])->name('post.delete');
    Route::post("/index", [PostController::class, 'save'])->name('post.save');
    Route::get('/categories', [CategoryController::class, 'list'])->name('category.list');
    Route::post('/categories', [CategoryController::class, 'save'])->name('category.save');
    Route::get('/categories/create', [CategoryController::class, 'create'])->name('category.create');
    Route::delete('/categories/{category}', [CategoryController::class, 'delete'])->name('category.delete');
    Route::put('/categories', [CategoryController::class, 'update'])->name('category.update');
    Route::get('/categories/edit/{category}', [CategoryController::class, 'edit'])->name('category.edit');
    Route::get('/categories/{category}', [CategoryController::class, 'show'])->name('post.show');
//});


Route::post('/index', [PostController::class, 'index'])->name('post.index')->middleware('auth:sanctum');

Route::post('/tokens/create', function (Request $request) {
    $user = Auth::user();
    $token = $user->createToken('testToken');

    return ['token' => $token->plainTextToken];
});




//1|x3XqJet6BLaB9hI7qdOZs1P8tdUHwGsFQmsAOAje
