<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Post;
use App\Models\User;
use App\Models\Tag;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

//        $post->tags()->attach(
//            Tag::all()->random(1, 3)->modelKeys()
//        );
//
//
//        Tag::factory(10)->create();



        User::factory(5)->create();
        Tag::factory(5)->create();
        Category::factory(5)->has(
            Post::factory(1))
            ->create()
            ->each(function($category){
                $category->posts->each(function($post){
                    $post->tag()->attach(
                        Tag::all()->random(rand(1,3))->pluck('id')->toArray()
                    );
                    $post->user()->associate(
                        User::inRandomOrder()->first()
                    )->save();
                });
            });
    }
}
