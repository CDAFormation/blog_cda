<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->word(5),
            'content' => $this->faker->realText(300),
            'img_url' => $this->faker->imageUrl(),
            'created_at' => now(),
            'updated_at' => now(),
        ];

    }
}
